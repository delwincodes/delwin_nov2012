title: Change
published: 2012-11-26

When I finally bought a MacBook Pro, I wasn't totally convinced that I needed it. My black 2008 MacBook had served me perfectly well for nearly 4 years. It ran okay. The built-in speakers didn't work, but I usually had them connected to stereos or headphones anyway. What finally convinced me to buy a MPB, though, was that feeling of freshness.

I knew I was going to upgrade to a Pro at some point, because I wanted to run Aperture without so much fan noise (and lots of other reasons). And after I began thinking about that, I put off the purchase for weeks. The final tipping point came when I got a paycheck in the mail and thought, well, why not?

It's a refurbished machine &mdash; and I try not to think of it as any more than a machine &mdash; but when I got it, opening it up, setting everything up, reorganizing my home folder...all of that felt so _fresh_. It was a good feeling. I felt free, I felt inspired and motivated, I wanted to _do good work_ on this new machine, not tarnish its reputation with laziness and complacency. 

### We need change

Perhaps it's just my youthful spirit, but I thrive on change. No, I don't think it's just my youthful spirit. I think most people have a sense of freshness when things change. That's why people buy new clothes, decorate their houses for the season, paint their walls new colors, plant a garden. Everyone has some way of giving a sense of freshness to their life, because it's rejuvinating.

Humans like having that control, they like feeling like their lives are progressing and things are different. The same is boring. Okay, okay, I'm done explaining all of this, because it's _so damn obvious_.

It's a tricky subject, because frequent change can be unnerving, or confusing. You don't want website redesigns all the time. You need your home folder directory structure to remain somewhat stable. You like your kitchen appliances in the same place most of the time, because it's hard to retrain muscle memory (although I think we exaggerate that inconvenience).

### Change as inspiration

All change should solve a problem, not simply be a whimsical alteration. Sometimes that problem is as simple as a lack of inspiration. Go camping for a week, and bring a notebook. You'll have a year's worth of ideas when you're done.

Most of the time, it doesn't need to be that drastic. If you're looking to be more inspired in your work, rearrange the furniture. It doesn't exactly matter how. Buy a chair; it's not too important what it's like, as long as it's _different_. 

If you run a small business, experiment with moving your employees (and yourself) around in the office, physically. Switch up their locations every few months. See what happens.

All that really matters is that you invest in inspiration by changing your surroundings. You're going to keep having the same old ideas if you're looking at the same old things. It's really that simple.

---

As an appendix, here's an list of things to change, assuming your workspace is computer-centered:

+ Change workspace decoration
+ Rearrange workspace furniture
+ Use new pens/pencils/notebooks
+ Buy new hardware (a bit expensive, though)
+ Change your operating system's look and feel (customization is somewhat limited on OS X, unfortunately)
+ Spend less time inside
+ Work in a coffeeshop
+ Paint your walls
+ Replace or cover your physical desk with a dry-erase board
+ Buy a different shape/type of desk.
+ If you're part of a small company, change offices or desks every couple months
+ Eat different things
+ Wear different clothes
+ Listen to different music
+ Burn incense

It's stimulation. It solves boredom.