title: Mirrors and Magnifiers
published: 2012-11-11

I was walking home from class today when the thought suddenly hit me that _I would be graduating next May._ My time at university would total four semesters upon graduation &mdash; just two years. I know graduating so early is rare, but I felt strange taking pride in it.

My head had briefly swelled with egotism as I was convinced &mdash; for a split second, and that's all &mdash; that I was superior to other students. I don't think that's true at all. I'm a liberal arts student. We can get A's without working too hard. 

### The easy way out

I'm on a pretty light path through higher education, to be honest. I'm not doing homework every night. The bulk of my studies consist of reading books I enjoy and writing papers about them. Occasionally I diagram a sentence for my _Syntax & Semantics_ class. It's really not that difficult.

I could be pushing myself harder, if I wanted to. But I find more use of my mind outside of school, where I'm programming and working on music, or reading without the limitations of education. I push myself harder than any school ever did. 

So it is clear to me immediately that my early graduation isn't due to fastidious study habits. I go to class so I don't have to study for tests. I thoroughly enjoy the readings, so papers are easy to write. School is effortless.

### Entrepreneurial endeavors

My faint entrepreneurial instincts suggested I take advantage of my uniqueness, that I had simply happened to put myself on a path to early graduation. In a moment's passion I bought _collegein2years.com_ and quickly set up a blog. I assumed I could write a bunch about how to graduate early, the best way to "hack the system" and get out early. Once I had written a large amount of content over nine months, I could compile it into an e-book and sell it for $20 a pop to fretful moms with college-bound kids.

I quickly lost my passion for that kind of writing. It was shallow and useless. I found myself spouting advice from a pedestal that enlarged my authority on the subject. It all felt very fake. And aside from that, the blog was directly contracting my eagerness to get out of school and throw off the hold academia has over me.

Plus, I had discovered [Angelica Kalika, who graduated from Berkeley in two years](http://berkeleyintwoyears.blogspot.com/) and was currently trying to [sell her story](http://www.amazon.com/gp/product/0615233015?ie=UTF8&seller=A30VJ9S4XZNGO4&sn=berkeleyintwoyears). I looked in her and saw all the things I didn't want to be: a former student, depending on her past for her future. Constantly thinking retrospectively. If I loved school that much, I certainly wouldn't be graduating so early.

### The dilemma & the future

> The intellect is vagabond, and our system of education fosters restlessness.

Not everything Ralph Waldo Emerson wrote 150 years ago still holds true, but this quote — something I just encountered for the first time, reading _Self-Reliance_ this afternoon — is among my favorites.

I like learning. I love my classes, and I can't wait for my last semester: fifteen hours of the most exciting classes I could find in the course schedule. I just find it hard to keep this up. I'm ready for something new. I'm restless.

So I've chosen to leave school early. I could handle another semester, and probably another after that, but the sentiment keeps growing. I need some time, and a new place, to explore my potential. 

And education will always be close at hand. I'm planning on enrolling in as many [Udacity](http://udacity.com/) and [Coursera](http://coursera.org/) as soon as I graduate.

---

Since I've been spending my entire college career engrossed in literature, culture, linguistics, and history, I'm tempted by the high-quality science courses I've seen offered online. Here are some of my favorites:

 + [Differential Equations](http://www.udacity.com/overview/Course/cs222/CourseRev/1)
 + [HTML5 Game Development](http://www.udacity.com/overview/Course/cs255/CourseRev/1)
 + [Parallel Programming](http://www.udacity.com/overview/Course/cs344/CourseRev/1)
 + [Sustainable Agriculture](https://www.coursera.org/course/sustainableag)
 + [Energy 101](https://www.coursera.org/course/energy101)
 + [Organic Chemistry](https://www.coursera.org/course/orgchem1a)
 + [Critical Thinking in Global Challenges](https://www.coursera.org/course/criticalthinking)
 + [Astrobiology](https://www.coursera.org/course/astrobio)
 + [Epigentic Control of Gene Expression](https://www.coursera.org/course/epigenetics)
 + [Animal Behavior](https://www.coursera.org/course/animalbehav)