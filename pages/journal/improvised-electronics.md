title: Improvised electronics
published: 2012-11-18

Music is vast. Musicians dig holes to house their music so they don't have to contend with the rest of the world. They can stay in their holes with similar-minded people, making music that everyone around them understands. There's a dialogue between the creators and the consumers, so everyone is on the same page.

Sometimes artists will travel between genres. Most of my friends who are musicians play a wide variety of music. I have played in bluegrass bands, jazz big bands, rock bands, funk bands, punk bands, and orchestras. I liked some more than others. But that's the key &mdash; once you break out of one hole, you can move between them. But you'll rarely be playing such a wide variety of genres within the same group; this is why "bands" exist. 

### What is a band?

A band is not simply a group of people who play music together. It is conceptual frameworks, it provides boundaries. A band is a territory that players enter. Although the players initially shape the territory, it soon begins to shape them, and bend them to its will. It is more than the sum of its members.

The reason why this is on my mind is because today I created some music entirely different from everything I've been doing previously. I sat down with a sheet of paper, thinking about what _values_ I want my music to incorporate. I worked out, conceptually, a project that would fit my values. It was deliberate, contrived, unlike the organic "play-what-you-feel" music I had made before. My list came out to something like this:

+ Improvisation
+ Repetition (e.g. loops)
+ Reference (e.g. samples)
+ Simulated organics

It was going to be an electronic project. I sat down with [Ableton Live](http://ableton.com/) and my MIDI controllers, and starting from blank templates, created what I called "Bilfristad":

<iframe width="100%" height="300" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=http%3A%2F%2Fapi.soundcloud.com%2Fplaylists%2F2775258&amp;color=ff6600&amp;auto_play=false&amp;show_artwork=true"></iframe>

It's unlike most things I've ever done, and right now, it's some of the shittiest music I've ever made. With time, it will get better.

### Improvisation: musical ideal

Perhaps it's because of all of the time I've spent playing jazz, and jamming with other musicians, but I've always found improvisation to be, to me, almost the definition of music.

The music I make cannot be separated from the moment, abstracted and repeatable. Perhaps this is why [my previous efforts in electronic music](https://soundcloud.com/enceladus/darvaza) have been so frustrating. I always enjoy the end result, but the composition phase feels unnatural sometimes. And all of the highly-produced electronic music I've created has always had some element of improvisation, even if it has been canned, prerecorded. Listening closely, my track [Darvaza](https://soundcloud.com/enceladus/darvaza) has plenty of similarities with the more improvised [Pink Tea](https://soundcloud.com/enceladus/bilfristad-pink-tea).

Improvisation represents the primal aspect of music, how I imagine it was invented. When there was no codified notation, no rules or styles. Improvisation is a visceral, highly emotional activity. You're filled with empathy, the music flows through you, and out of you, and then it's gone. Composed music lacks that element. Music loses much of its spirit when you separate its conception from its performance.

### Please stop rambling

All of this is to say:

+ A band is a concept that is brought to life by a group of people, not the actual group of people itself
+ When a new concept arises, a group of people joins to embody it (this is something I've learned through observation of my friends)
+ Improvisation is one of the most important values music can have, for me. It's all subjective, though. I wouldn't expect your tastes to match up to mine perfectly.

And it was a humble way to bookend a promotional of my new project, [Bilfristad](https://soundcloud.com/enceladus/sets/bilfristad).
