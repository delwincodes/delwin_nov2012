title: A Curicculum for Modern Web Development
published: 2012-11-21

The web is becoming increasingly interactive, increasingly mobile, and increasingly complex. I've been lucky to learn the new stuff as it comes, in other words, without so much "catching up." But for those of you just starting out, it can be daunting. This post will hopefully give you a path to follow.

### Level 1: How to form webpages

Learning how to code the web nowadays is a lot more difficult than it was when I started in 2005. And even by 2005, it was a lot more difficult than it was in 1995. But you'll still be starting with the same basic stuff.

+ **HTML** Easy. Until you get to the new features introduced with version 5, HTML is a pretty easy thing to wrap your head around.
	
+ **CSS:** Easy, and very straightforward. You'll be learning CSS tricks your whole life, because the possibilities are endless, but the concept is simple.

Give yourself a weekend and learn HTML and CSS inside and out. 

**Resources:** 

+ [Mozilla Developer Network](https://developer.mozilla.org/en-US/)
+ [Codecademy HTML & CSS track](http://www.codecademy.com/tracks/htmlcss) - interactive course

### Level 2: Simple applications

HTML and CSS get tiring, and redundant. To stop repeating header and footer code, you can learn to make templates. This is done most easily in PHP, since it runs on almost any webserver, and all you need to know is one function: `include()` and your life becomes so much easier.

Many people will tell you PHP is terrible. Mostly, they're right. It's not fun to program in PHP. You will probably never love PHP. But it's simple, and runs anywhere.

After level 2, you won't have to use it anymore. Promise.

+ **PHP:** It's useful to know, for any web developer in any situation, just like Linux is good to know. Because it's used everywhere. Don't worry too much about delving deeply into PHP, though, because you will likely not want to spend your life married to it.
+ **MySQL:** A good introduction to SQL. PHP and MySQL also work well together, and there is a huge number of books on the two combined.

I recommend spending 1-3 weeks on PHP and MySQL, to get the hang of it. All of the websites you've made in HTML and CSS you can now extend with databases, contact forms, newsletters, archives, blogs, etc.

**Resources:**

I learned PHP and MySQL from a book. Your local library undoubtably has one. It doesn't matter if it's a bit old, the fundamentals haven't changed much at all.

Also, consider picking up WordPress. It's built on PHP and MySQL, and is also good to know, since so many people are using it. Lots of my client work ends up being made with WordPress. This blog used to be WordPress, until I [switched to Flask](/journal/powered-by-flask/).

### Level 3: Basic JavaScript

This is where things start to get interesting. Once your sites have a bit of front-end and back-end structure, you can start picking sides and digging deeper into the cooler stuff. In Level 3, we're starting with the front-end. Learn JavaScript and jQuery.

+ **JavaScript** is a big language. You can do a lot with it. I recommend starting with the basics (see resources below).
+ **jQuery** is immediately useful for all developers. Lots of people recommend you should learn JavaScript fully before starting on jQuery. I disagree. It's incredibly useful, far more practical, and enchantingly delightful for the beginner. Once you have a firm hold on the _basics_ of JavaScript, move on to jQuery. You'll come back to learn advanced JS later.

I recommend a month of solid study to get basic JavaScript and jQuery under your belt. It really comes down to how much you use it.

**Resources:**

+ [Codecademy JavaScript track](http://www.codecademy.com/tracks/javascript-combined) - interactive course
+ [Mozilla JavaScript docs](https://developer.mozilla.org/en-US/docs/JavaScript)
+ [jQuery Air](http://www.codeschool.com/courses/jquery-air-first-flight) - interactive course

### Level 4: Full-fledged applications

Now's the time to break out of PHP, and start doing cooler stuff. Build applications that solve real problems. Write a blog engine first. Then write a time-tracking and invoicing application. Keep making cool stuff, applying everything you've learned, Googling like mad to learn new stuff.

+ **Basic Ruby or Python:** You could stick with PHP, but Ruby and Python are much friendlier. If you don't know which to choose, choose the one your friends know, so you have something you can talk about with them. It really doesn't matter at this stage in the game. Ruby seems to have slightly more hold on the Silicon Valley startup community, if that's your thing.
+ **Web application framework:** There are so many choices. If you chose Ruby, you'll have to learn [Rails](http://rubyonrails.org/), because it's ubiquitous. If you choose Python, you can go with [Django](https://www.djangoproject.com/), or a smaller framework like [Flask](http://flask.pocoo.org/) or [web.py](http://webpy.org/). The larger frameworks are a little more complicated to learn (in my opinion), but are well documented and represented online, so finding resources is a little easier. It's really up to you.

Spend 1-3 months building cool stuff with your new framework. If you spend a couple weeks learning the language, that's enough. Then start building applications.

This is kind of like learning jQuery and JavaScript. Learn the basics of the language, but then move on quickly to a framework so you can get projects done, which will impress you and motivate you to continue. They won't be perfect, or even presentable, but they'll be super cool.

**Resources:**

+ [Try Ruby](http://tryruby.org/levels/1/challenges/0) - interactive course
+ [Codeschool Ruby/Rails courses](http://www.codeschool.com/courses/tag/ruby) - interactive course
+ [Codecademy Python track](http://www.codecademy.com/tracks/python) - interactive course
+ [Udacity CS101 (Python)](http://www.codecademy.com/tracks/python) - interactive course
+ [The Django Book](http://www.djangobook.com/en/2.0/index.html)
+ [Ruby on Rails Guides](http://guides.rubyonrails.org/)
+ [Ruby on Rails Tutorial](http://ruby.railstutorial.org/ruby-on-rails-tutorial-book) - the best resource I've found for learning Rails
+ [Flask docs](http://flask.pocoo.org/docs/)
+ [Web.py cookbook](http://webpy.org/cookbook/)

### Level 5: Improve your workflow

At this point, it's about building stuff, learning best practices, reading a lot, coding a lot, etc. But I recommend you take a week or two to learn some tools that will greatly affect your workflow. It's good to get them in early, before you're too entrenched in your ways to change them.

+ **Git:** Learn version control. I recommend Git, because it's trendy and easier to use than the alternatives. GitHub is ubiquitous. 
+ **Sass/LESS:** A CSS pre-processor will improve your stylesheets _so so so much_. You don't believe me, until you change to using Sass or LESS (Sass seems to pull more weight nowadays), but believe me, they'll make a huge difference.
+ **CoffeeScript:** A JavaScript pre-processor. I never got into it, but perhaps one day someone will convince me to. One day it will be unavoidable, just like Sass and LESS.
+ **HAML:** If you're using Ruby on Rails, learn HAML. It will make templating a breeze, and so much fun.
+ **HTML5:** HTML5 is a pretty large set of new technology, but now's a good time to start playing around with it. Learn the basics, and come back to it when you need it.
+ **Terminal:** If you haven't yet, now's the time. Learn the basics. Learn how to use vim or some terminal-based editor so you can make quick fixes via SSH.

This is an easy level, I guess. A lot of logistical stuff, but it should help you out a lot.

**Resources:**

+ [GitHub](http://github.com/) - use Git on all your projects! All of them!
+ [BitBucket](https://bitbucket.org/) - free private repos, unlike GitHub. Supports Git.
+ [Sass](http://sass-lang.com/)
+ [LESS](http://lesscss.org/)
+ [HAML](http://haml.info/)
+ [CoffeeScript](http://coffeescript.org/)
+ [HTML5 Rocks](http://www.html5rocks.com/en/) - awesome resource!
+ [CodeKit](http://incident57.com/codekit/) - to help with your pre-processors
+ [Sublime Text](http://www.sublimetext.com/) - the best editor I've ever come across
+ [Linuxcommand.org](http://linuxcommand.org/lc3_learning_the_shell.php) - learn the shell. Most *nix command line references are relevant for Mac users too

### Level 5: Advanced front-end

This is where my skillset starts to get a little spotty. I know some of this stuff, but not all of it. From here, this is kind of a "to do if you haven't already" list for me.

+ **HTML5 Gaming:** When I started coding computer games, when I was 13, I learned _so much_ about programming. They're such huge projects, they reach to all corners of a programming language and teach you things you'd never thought of learning on your own, out of context. This is that, but instead of Blitz Basic, it's for the web.
+ **Backbone.js:** I'm told it's super useful, but I haven't had the chance to try it out yet.
+ **Responsive design:** Invaluable. And not difficult at all.

There's a lot I could list here, but at this point, the track starts to get more specific. By the time you're here, you'll know your interests. You'll be improving your JavaScript, jQuery, HTML, and CSS skills. You'll be working on making websites work on your phone. 

It's a mess at this stage, with paths leading in every direction. Just find a project and build it. 

**Resources:**

+ [HTML5 Rocks, again](http://html5rocks.com/)
+ [Backbone.js](http://backbonejs.org/)
+ [Codeschool's Backbone course](http://www.codeschool.com/courses/anatomy-of-backbonejs)
+ [Introduction to Responsive Web Design](http://blog.teamtreehouse.com/beginners-guide-to-responsive-web-design)

### Level 6: Advanced back-end

Not much to say here. Just build increasingly trickier and advanced stuff in your language of choice. Some things I suggest:

+ **Browse GitHub** all the time. Look at other people's code. It's incredibly useful.
+ **Listen to podcasts**, even while you code. 
+ **Find a meetup** in your city, share your creations with other programmers, and learn from them.
+ **Subscribe to the [Hacker Newsletter](http://www.hackernewsletter.com/)**, just to stay on top of the ball.

### Level 7: Hardcore code

About now is when you should be reaching back and learning the basics of the languages you skipped over. Now's the time to _really_ learn how to code, learn how programming languages work and how the web fits together.

+ **Master a programming language:** any one will do. Became a JavaScript master, a Python master, a Ruby master. This will take some serious time. A year, at least, and that's if you're working _really friggin' hard_.
+ **Get a job:** Once you can code decently well, get a job that challenges you. Build sites for clients. Reach out and make connections. You'll need them later.
+ **Learn how the web works.** There's really no way to do this other than immersing yourself in it, paying attention and reading a lot. 

**Resources:**

+ [Udacity](http://udacity.com) has some great programming courses
+ [InterviewStreet](http://www.interviewstreet.com/) - language-agnostic code challenges

### Level 8: Build cool stuff

At this point, the only thing you can do is code. The new technology that comes out feels easier, because you can instantly understand how it works, what it does. Some coders never reach this stage, because it's not necessary to get here in order to make a living. 

And by the time you're here, you will have long forgotten this post. It will be irrelevant. Thus, I don't need to write anymore.

### Why I wrote all this

A couple reasons:

1. To have an article to send people to who ask me, "How do I get started as a web developer?"
2. To have a roadmap for myself. I've been complacent recently, just coding sites for clients, not seeking out new things to learn. It is time for that to change. I'm starting with Backbone.js because it looks super cool.
3. With the hope that some curious teenager or confused twentysomething will come across my website and be suddenly inspired to create something awesome for the internet.

That's all, really. Almost 2000 words. I should really get some work done.