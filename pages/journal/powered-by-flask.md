title: Powered by Flask
published: 2012-11-07

I've been running [my personal site][delwin] on WordPress for most of it's lifetime, and it was about time for something new. I tried Ruby on Rails, after picking it up for the hype, but it was overkill for this project. I eventually settled on [Flask][], and I couldn't be happier with the result. Let me explain how it all works…

### Look ma, no database!

Even using a database for my personal site seemed like overkill. I'm not going to be serving any large amount of data. This site will consist of journal entries (like this one) and static pages. 

I found a plugin for Flask called [FlatPages][] which turns simple Markdown-formatted text files into content that can be served "dynamically" by Flask. This is essentially what is powering the site:

	@app.route('/<path:path>/')
	def page(path):
		page = pages.get_or_404(path)
		return render_template('page.html', page=page)

`pages` is a FlatPages object, and handles pretty much everything. The entire application &mdash; excluding templates, and the core Flask code &mdash; is only 42 lines, in one file. That file also contains two custom Jinja template filters.

One of them takes the first paragraph of each post as an "excerpt" to display on the front page:

	def excerpt(value, limit=1):
		words = value.split('\n')
		return (" ".join(words[:limit]))

The other formats the date. I found this one on the Jinja website:
	
	def datetimeformat(value, format="%d %B %Y"):
		return value.strftime(format)
		
[FlatPages][] also allows metadata using PyYAML. Above the content of this article are two lines of metadata:

	title: Powered by Flask
	published: 2012-11-07
	
Adding more metadata is as easy as:
	
	tags: [flask, code, webdev]
	
And dates are automatically converted to Python `datetime` objects, for easy manipulation &mdash; the Jinja `datetimeformat` filter works seamlessly.
		
### Git 'er done

I started development of the site with a Bitbucket-powered Git repo, which means my site is backed up to the cloud after every revision.

I can write blog posts from anywhere (though I prefer to edit them on my laptop with [Mou][]) via SSH, and have the changes synced to Bitbucket and back to my computer. Everything stays up-to-date.

The site is also easily deployed via Git. I seriously regret not doing this before. It's insanely easy. I think every site I build in the future will be deployed this way.

This site is unique among my web development projects, though. This one has no rules. It's a laboratory, a sandbox. I'm going to be digging around under the hood and adding new features all the time because Flask makes it _so damn easy_.

### A return to writing

All this geekery aside, what I'm really interested in doing is writing. Writing about code, about food, about the future, about travel, about photography and art and literature and languages. I'm kind of all over the place. But this is my site, and I have no one I need to please.

[delwin]: http://delw.in/
[Flask]: http://flask.pocoo.org/
[FlatPages]: http://packages.python.org/Flask-FlatPages/
[Mou]: http://mouapp.com/