title: Food Systems
published: 2012-11-15

I like systems and cycles and self-contained collections of moving parts that work well together. That's why I'm so excited about [the way this blog works](/journal/powered-by-flask) — it's backed up in three places, synced by git, and I don't have to deal with any databases. But what if I could apply my love of systems to food?

### Homebrewing

One of the easiest examples I can think of is homebrewing. Not just beer or wine, but anything fermented. I like the idea that you can put bacteria (most often yeast) in a container with sugar and water and it turns into something else, with time. It's magical.

Right now I'm brewing [water kefir](http://nourishedkitchen.com/water-kefir/), which is one of the msot delicious drinks I've ever had. Although it's similar to kombucha, I found water kefir much more palatable. It's a sweet, slightly fermented fizzy fruit drink. But the coolest thing about it, by far, is that I have very little hand in creating it. I simply set the fermentation in motion (which takes just a few minutes to prepare), and in two days, I have huge amounts of delicious drink. The bacteria infinitely replicate as long as they have sugar to eat, so my production of kefir is, for all practical purposes, limitless.

### Self-sufficiency

It's important to distinguish this type of food preparation from simple cooking. It's not about turning a set of ingredients into a meal. It's about turning ingredients into other ingredients, that can be stored a long time. It's about working from only raw materials; using only what nature provides to create complex new items. It's **cooking+**.

Here are some other examples, to give you an idea of what I mean by **cooking+**:

 + Pickling okra or cucumbers
 + Making jam / fruit butter
 + Making granola 
 + Making sauces, infused oils, etc.
 + Baking bread — not just special breads, but _all_ your bread

Short from farming all the raw materials myself (which is something I hope to do further down the line), I'd like to get to the point where I'm not buying prepared food anymore. I buy the raw, local materials, when they're in season, and combine them into new things.

### Food systems

So what is the food analogue of all the things I love about this website? I think it is a self-contained system, a cycle that receives seasonal food as input and creates, as output, a subjective selection of _food products_ that I enjoy.

As an example, one shopping trip might yield:

 + Oats
 + Honey
 + Bell peppers
 + Tomatoes
 + Mushrooms
 + Garlic
 + Raspberries
 + Cucumbers
 + Peaches
 
With staples I always have at home, after a little preparation, these ingredients would yield:

 + A large amount of homemade honey-oat-nutmeg granola.
 + "Zesty" dill pickles, because I like them zesty.
 + Homemade marinara sauce
 + Raspberry-peach soda, courtesy of my water kefir factory.
 
The idea is to have a predictable cycle of food items to work with. They won't always be the same — fresh fruit will be favored over jam in the summertime, for example — but the concept remains constant.

### Creativity

The crucial distinction is that in this food system, food products are bought not to suit a diet or a particular recipe. Rather, food products are bought based on availability, then transformed into newer products, which can be combined to make whatever is possible.

This means what you actually end up eating is up to you. You start from scratch every time. If you want a peanut butter and jelly sandwich, you start with peanuts, strawberries, flour (my daydreams have never included the tedious act of turning grain into flour), sugar, salt, yeast, and whatever else you want in your bread (butter, milk, honey, etc.). The strawberries can become jam easily. The peanuts can become butter [even more easily](http://www.wikihow.com/Make-Peanut-Butter). The bread takes a couple hours to bake. Once you're done with all of this, I can guarantee you're _really going to appreciate that sandwich_. It's going to be the best PB&J you've ever had, even if it isn't.

The creativity in my food system comes from the limits you impose on yourself by locking your consumption into natural cycles and seasonal availability. Limits are useful creativity-enhancers when writing, making art, or playing music, and the same is true for food. I never know what I should cook if I show up at Whole Foods. There are too many foods. But if I take a look in my pantry and see a homemade garam masala spice mix and lentils, and my refrigerator is overflowing with delicious Fredricksburg tomatoes, then naturally I'm going to make [dal](http://en.wikipedia.org/wiki/Dal). If it's chickpeas, I'll make some falafel balls. Decisions become very easy to make.

### This is all a very rough idea

I'm not sure that _food system_ is even an appropriate name for this sort of attitude towards food, and I'm aware my daydream is not unique. But there is a certain point of view I'm hoping for.

The change of perspective comes with me not seeing myself outside the ecosystem, but as a very important part of it. We humans do not sit on top of, or outside of, the food cycle. We are integrated into the whole system. What we consume affects everything else.

I feel that by processing foods myself, instead of having them processed for me at a factory, I can immerse myself in this system. It enriches my whole experience of food. When you can see natural bounty turn into things that look nothing like what they used to, it's magical. Food becomes something that _can_ be experienced, not just tasted. Food becomes sacred.