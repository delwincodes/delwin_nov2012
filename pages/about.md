title: About
date: 2012-11-04
nav: True

Hey everybody. I'm Delwin and this is my website. 

### me

Since this is the internet and paragraphs are largely ignored, here are some bullet points about me:

+ I am a **musician**. I play jazz piano, drums, and ukulele, though I'm best at piano. I also produce electronic music. [You can hear it on SoundCloud](http://soundcloud.com/enceladus/). I am completely avid about music in all its forms.
+ I am a **web designer & developer**. Right now, mostly a developer. But I'm working on my design skills. My current skillset leans towards front-end development.
+ I am a **writer** and sometimes even a poet. I feel awfully egotistical referring to myself as a writer, though, I prefer "person who writes things." Same with my other "titles." Should people be defined by their _character_, their _actions_, or their _associations_? Aren't they all the same, in the end?
+ I like to **learn languages**. Currently, I speak English, Swedish, Spanish, and French, at various levels of competency. I have extremely basic, beginner knowledge of German, Chinese, and Esperanto.
+ I love food and cooking. I eat quite heathily, about 85% vegan, but without all the frozen or processed soy products.

### what I do

+ I make websites for people [under the name No Enemies](http://noenemies.com/).
+ [AbletonOp](http://abletonop.com/), a blog about producing electronic music with Ableton Live
+ [Earth is Awesome](http://earthisawesome.com/), a bi-weekly newsletter that rounds up the best of the internet, with a global focus
+ Electronic dance music [under the name Enceladus](http://soundcloud.com/enceladus).
+ [Avant-garde improvised electronic music](http://soundcloud.com/enceladus/sets/bilfristad) under the name Bilfristad
+ My [multilingual journal](http://lang-8.com/330625/journals). Largely uninteresting.
+ My [code on GitHub](http://github.com/enceladus)
+ [I tweet](http://twitter.com/thecampbell) occasionally, too.

### what about the site?

It was built in Flask, without a database. All content is static Markdown files. The site is distributed/published/edited via Git. [Here's the post](/journal/powered-by-flask/) describing the process. 

I think I will turn it into a self-contained blog engine at some point, to make the easy process even easier.