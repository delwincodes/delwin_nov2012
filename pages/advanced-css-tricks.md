title: Advanced CSS Tricks Checklist

This is a list of things to learn how to do in CSS. It starts at a pretty high intermediate level, assuming you know the basics.

### Positioning/layout/syntax

+ [Flexbox](https://developer.mozilla.org/en-US/docs/CSS/Using_CSS_flexible_boxes)
+ [Columns](http://www.quirksmode.org/css/multicolumn.html)
+ [Media queries](https://developer.mozilla.org/en-US/docs/CSS/Media_queries)
+ [@font-face](https://developer.mozilla.org/en-US/docs/CSS/@font-face)
+ [@supports](https://developer.mozilla.org/en-US/docs/CSS/@supports)
+ [Advanced attribute selectors](http://coding.smashingmagazine.com/2009/08/17/taming-advanced-css-selectors/)
+ [Pseudo-classes](http://css-tricks.com/pseudo-class-selectors/)


### Static visuals

+ [Multiple backgrounds](https://developer.mozilla.org/en-US/docs/CSS/Using_CSS_multiple_backgrounds)
+ [Gradients](https://developer.mozilla.org/en-US/docs/CSS/Using_CSS_gradients)
+ [background-size](https://developer.mozilla.org/en-US/docs/CSS/Scaling_background_images)
+ [3D transforms](http://desandro.github.com/3dtransforms/)
+ [More 2D & 3D transforms](https://developer.mozilla.org/en-US/docs/CSS/transform)
+ [Text selection color](http://www.designjuices.co.uk/2010/09/css3-tutorial-how-to-change-default-text-selection-colour/)

### Animation

+ [CSS animation basics](https://developer.mozilla.org/en-US/docs/CSS/Using_CSS_animations)

### Effects

 + [Parallax (with JavaScript)](http://net.tutsplus.com/tutorials/html-css-techniques/simple-parallax-scrolling-technique/)
 + [Parallax (CSS only)](http://www.creativejuiz.fr/trytotry/css3-parallax/#s1) - the best tutorial I could find is [in French](http://www.creativejuiz.fr/blog/tutoriels/css3-effet-parallaxe-sans-javascript)
 + [filter](http://lab.simurai.com/stars/)