from flask import Flask, render_template
from flask_flatpages import FlatPages

DEBUG = True
FLATPAGES_AUTO_RELOAD = DEBUG
FLATPAGES_EXTENSION = '.md'

app = Flask(__name__)
app.config.from_object(__name__)
pages = FlatPages(app)

# Formatting dates
def datetimeformat(value, format='%d %B %Y'):
    return value.strftime(format)
app.jinja_env.filters['datetimeformat'] = datetimeformat

def excerpt(value, limit=1):
	words = value.split('\n')
	return (" ".join(words[:limit]))
app.jinja_env.filters['excerpt'] = excerpt

# Articles (journal entries) are pages with a publication date
articles = (p for p in pages if 'published' in p.meta)
# Articles sorted by date
latest = sorted(articles, reverse=True, key=lambda p: p.meta['published'])

## PAGES ##
@app.route('/')
def index():
	return render_template('index.html', articles=latest[:10], body_class='home')

@app.route('/writing/')
def archives():
	return render_template('archives.html', articles=latest, body_class='single')

@app.route('/<path:path>/')
def page(path):
	page = pages.get_or_404(path)
	return render_template('page.html', page=page, body_class='single')

if __name__ == '__main__':
	app.run(port=8000)
